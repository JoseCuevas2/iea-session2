#!/usr/bin/env python3
import re
import sys 

def add_s(x):
    x = x + 's'
    return(x)

def pluralization(x):
    length = len(x)
    if re.search('[sxz]$', x):
        x = x + 'es'
    elif re.search('h$',x):
        if x[-2] in 'aeioudgkprt':
            x = add_s(x)
        else:
            x = x + 'es'
    elif re.search('y$',x):
        if x[-2] in 'aeiou':
            x = add_s(x)
           # x = x[:length -1] + 'ies'
        else:
            x = x[:-1] + 'ies'
           # x = add_s(x)
    else:
        x = add_s(x)

    return(x)

for idx, arg in enumerate(sys.argv):
    print("arg %d is %s" % (idx, arg))

x = str(sys.argv[1])

print(pluralization(x))


import unittest
import calcfunc


def Test_Calcfunc(TestCase):

    def test_add(self):
        expected = 3
        result = calcfunc.add(1,3)
        self.assertEqual(result, expected)
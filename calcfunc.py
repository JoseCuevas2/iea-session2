#!/usr/bin/env python3
import sys

class Calculator:
    def __init__(self):
        self.total = 0
        #print(total)


    def display(self):
    #   self.total = self 
    #    result = total
    #    self.total = result
        print('Total = ', self.total) 


    def clear(self):
        #self.__init__(self)
        self.total = 0
        #print('Total = ',result)
        return self.total 

    def eval(self, x, y, operator):
        if operator == '+':
            self.add(x, y)
        elif operator == '-':
            self.subtract(x, y)
        elif operator == '*':
            self.multiply(x, y)
        elif operator == '/':
            self.divide(x, y)
        else:
            print('Hi, hit else')


    # def log(self, x, y):
    #     result = math.log(x)/math.log(y)
    #     self.total = result
    #     return result


    def add(self, x, y=0):
        if y == 0:
            result = self.total + float(x)
        else:
            result =  float(x) + float(y)
        self.total = result 
        #print('Total = ',result)
        return self.total


    def subtract(self, x, y=0):
        if y == 0:
            result = self.total - float(x)
        else:
            result = float(x) - float(y) 
        self.total = result
        #print('Total = ',result)
        return self.total

    def multiply(self, x, y=0):
        if y == 0:
            result = self.total * float(x)
        else:
            result = float(x) * float(y)
        self.total = result
        #print('Total = ',result)
        return self.total

    def divide(self, x, y=0):
        try:
            if y == 0:
                result = self.total / float(x)
            else:
                result = float(x) / float(y)
            self.total = result
            #print('Total = ',result)
            return self.total
        except ZeroDivisionError:
            print('ZeroDivisionerror, cannot divid by 0')



# def calculate_args(x,y,operator):
#     print('Program arguments', sys.argv)

# def calculate(x,y,operator):

#     result = 0
#     try:
#         if operator == 'log':
#             try:
#                 result = math.log(x)/math.log(y)
#             except ValueError:
#                 print('math domain error')
#             except TypeError:
#                 print(' must be a real number, not string')

#         elif operator == '+':
#             result = (x + y)

#         elif operator == '-':
#             result = (x - y)

#         elif operator == '*':
#             result = (x * y)

#         elif operator == '/':
#             try:
#                 result = (x / y)
#             except ZeroDivisionError:
#                 print('ZeroDivisionError, cannot divide by 0')


#     except ValueError:
#         print('math domain error')

#     else:
#         print('made it to the else')
#     finally:
#         print('made it to finally')

#     return(result)

# for idx, arg in enumerate(sys.argv):
#     print("arg %d is %s" % (idx, arg))

# x = float(sys.argv[1])
# y = float(sys.argv[2])
# operator = sys.argv[3]

# print(calculate(x,y,operator)) 


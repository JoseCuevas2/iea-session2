#!/usr/bin/env python3 

import re
import sys

def searcher(filename,pattern):

    linenum = 0
    for line in open(filename):
        linenum += 1
        if re.search(pattern, line):
           print('Line number is: ',linenum,', item searched: ', pattern, ', line is:', line, end='')

    return
    
for idx, arg in enumerate(sys.argv):
    print("arg %d is %s" % (idx, arg))

filename = str(sys.argv[1])
pattern = str(sys.argv[2])

searcher(filename,pattern)

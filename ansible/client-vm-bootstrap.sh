# Install docker
sudo apt-get update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install -y docker-ce

# Add user to docker group
sudo usermod -aG docker ${USER}

# Install editing tools
sudo apt-get install -y git vim screen emacs
echo "hardstatus alwayslastline \"%{=b}%{G} %{b}%w %=%{kG}%C%A  %D, %M/%d/%Y \"" > ~/.screenrc

# Install Python
sudo apt-get install -y python3 python3-pip

# Install kubectl
sudo apt-get install -y kubectl bash-completion
mkdir -p ~/.kube
kubectl completion bash > ~/.kube/completion.bash.inc
echo "source ~/.kube/completion.bash.inc" >> $HOME/.bashrc

# Allow password ssh
sudo sed -i=.bak '/PasswordAuthentication/ c PasswordAuthentication yes' /etc/ssh/sshd_config
sudo systemctl restart sshd

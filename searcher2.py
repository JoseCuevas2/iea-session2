#!/usr/bin/env python3

import sys

def searcher2(filename,pattern):
    try:     
        linenum = 0
        for line in open(filename):
            linenum += 1
            if re.search(pattern, line):
                print('Line number is: ',linenum,', item searched: ', pattern, ', line is:', line, end='')
    except FileNotFound:
        print('FileNotFound, check to make sure ', filename, ' is real') 
        return

for idx, arg in enumerate(sys.argv):
    print("arg %d is %s" % (idx, arg))

filename = str(sys.argv[1])
pattern = str(sys.argv[2])

searcher2(filename,pattern)

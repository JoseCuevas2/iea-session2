#!/usr/bin/env python  

from calculate_args import calculate

#import calculate_args
import sys

for idx, arg in enumerate(sys.argv):
    print("arg %d is %s" % (idx, arg))

x = float(sys.argv[1])
y = float(sys.argv[2])
operator = sys.argv[3]

print(calculate(x,y,operator)) # use this for from calculate_args import calculate 

#print(calculate_args.calculate(x,y,operator)) #use this if you use import module 

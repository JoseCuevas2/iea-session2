#!/usr/bin/env python
import sys

def calculate_args(x,y,operator):
    print('Program arguments', sys.argv)

def calculate(x,y,operator):

    result = 0
    try:
        if operator == 'log':
            try:
                result = math.log(x)/math.log(y)
            except ValueError:
                print('math domain error')
            except TypeError:
                print(' must be a real number, not string')

        elif operator == '+':
            result = (x + y)

        elif operator == '-':
            result = (x - y)

        elif operator == '*':
            result = (x * y)

        elif operator == '/':
            try:
                result = (x / y)
            except ZeroDivisionError:
                print('ZeroDivisionError, cannot divide by 0')


    except ValueError:
        print('math domain error')

    else:
        print('made it to the else')
    finally:
        print('made it to finally')

    return(result)

for idx, arg in enumerate(sys.argv):
    print("arg %d is %s" % (idx, arg))

x = float(sys.argv[1])
y = float(sys.argv[2])
operator = sys.argv[3]

print(calculate(x,y,operator)) 

